### Repository is readonly

Development moved to https://github.com/airenas/list

# LiST

Transkribatoriaus IT sprendimas

Repozitorijoje yra:

- [Kaldi transkribavimo skriptai](src/decode),
- [dokumentacija](architecture),
- [diegimo skriptai](deploy/README.lt.md),
- [konteinerių paruošimas/kompiliavimas](deploy/local/README.lt.md).

Transkribatoriaus servisų kodas [bitbucket.org/airenas/listgo](https://bitbucket.org/airenas/listgo). Servisų API aprašymas: [čia](https://app.swaggerhub.com/apis/aireno/Transkipcija/)

---

## Autorius

Airenas Vaičiūnas

- [bitbucket.org/airenas](https://bitbucket.org/airenas)

- [linkedin.com/in/airenas](https://www.linkedin.com/in/airenas/)

---

## Licencija

Copyright © 2020, [Airenas Vaičiūnas](https://bitbucket.org/airenas).
Released under the [The 3-Clause BSD License](LICENSE).

The project uses other licensed software. See [Licenses](Licenses/).

---
