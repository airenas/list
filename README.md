### Repository is readonly

Development moved to https://github.com/airenas/list

# LiST

[Lietuviškai](README.lt.md)

Lithuanian Speech Transcription

The repository contains:

- scripts for transcription using Kaldi,
- architecture documentation,
- deployment scripts.

For transcription service code look at [bitbucket.org/airenas/listgo](https://bitbucket.org/airenas/listgo)

---

## Author

Airenas Vaičiūnas

- [bitbucket.org/airenas](https://bitbucket.org/airenas)

- [linkedin.com/in/airenas](https://www.linkedin.com/in/airenas/)

---

## License

Copyright © 2020, [Airenas Vaičiūnas](https://bitbucket.org/airenas).
Released under the [The 3-Clause BSD License](LICENSE).

The project uses other licensed software. See [Licenses](Licenses/).

---
